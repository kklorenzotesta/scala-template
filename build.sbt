resolvers += Resolver.sonatypeRepo("releases")

//addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0")
//addCompilerPlugin("com.lihaoyi" %% "acyclic" % "0.2.0")

//val circeVersion = "0.13.0"

lazy val root = (project in file("."))
  //.enablePlugins(ScalaJSPlugin)
  .settings(
    organization := "com.kklorenzotesta",
    scalaVersion := "2.13.3",
    name := "MyProject",
    mainClass in Compile := Some(
      "com.kklorenzotesta.myproject.Main",
    ),
    scalacOptions --= Seq(
      "-Wunused:params",
      "-Wvalue-discard",
    ),
    /*scalacOptions ++= Seq(
      "-P:acyclic:force",
    ),*/
    //autoCompilerPlugins := true,
    //dependencyUpdatesFailBuild := true,
    //scalaJSUseMainModuleInitializer := true,
    autoAPIMappings := true,
    //libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "1.1.0",
    //libraryDependencies += "org.typelevel" %%% "cats-core" % "2.2.0",
    /*libraryDependencies ++= Seq(
      "io.circe" %%% "circe-core",
      "io.circe" %%% "circe-generic",
      "io.circe" %%% "circe-generic-extras",
      "io.circe" %%% "circe-parser",
    ).map(_ % circeVersion),*/
    // This dependency is needed on order to fix a circe dependency on java time
    //libraryDependencies += "io.circe" %%% "not-java-time" % "0.2.0",
    //libraryDependencies += "com.lihaoyi" %% "acyclic" % "0.2.0" % "provided",
  )
